var c=0;
var t;
var tens = {10 : 'Ten', 20 : 'Twenty', 30 : 'Thirty', 40 : 'Forty', 50 : 'Fifty'};
var ones = { 20 : '', 0: 'Zero', 1 : 'One', 2 : 'Two', 3 : 'Three', 4 : 'Four', 5 : 'Five', 6 : 'Six', 7 : 'Seven', 8 : 'Eight', 9 : 'Nine', 10 : 'Ten', 11 : 'Eleven', 12 : 'Twelve', 13 : 'Thirteen', 14 : 'Fourteen', 15 : 'Fifteen', 16 : 'Sixteen', 17 : 'Seventeen', 18 : 'Eighteen', 19 : 'Nineteen'};

function timedCount() {
	var dateTime = new Date();
	var hour = dateTime.getHours()%12;
	var mins = dateTime.getMinutes();
	var secs = dateTime.getSeconds();
	var className = 'b c';
	for (var i=1; i<11;i++ ) {
		addClass(i, 0, 11, className);
	}
	className = 'a';
	addClass(1, 0, 2, className);
	addClass(1, 3, 2, className);
	addClass(10, 5, 6, className);

	className = 'a b';
	if (mins > 19) {
		minString = tens[mins - (mins%10)] + ' ' + ones[mins%10 == 0 ? 20 : mins%10];
	} else {
		minString = ones[mins%20];
	}
	if (hour > 19) {
		hourString = tens[hour - (hour%10)] + ' ' + ones[hour%10];
	} else {
		hourString = ones[hour%20];
	}
	if (secs > 19) {
		secsString = tens[secs - (secs%10)] + ' ' + ones[secs%10 == 0 ? 20 : secs%10];
	} else {
		secsString = ones[secs%20];
	}
	x = hour;
	y = parseInt((mins%30)/5);
	z = parseInt(mins/30);
	if (z>0 && y!=0 )
	{
		x +=1;
	}
	x = x == 0  ? 12 : x;
	switch(x) {
		case 1 :	addClass(6, 0, 3, className);
					break;
		case 2 :	addClass(7, 8, 3, className);
					break;
		case 3 :	addClass(6, 6, 5, className);
					break;
		case 4 :	addClass(7, 0, 4, className);
					break;
		case 5 :	addClass(7, 4, 4, className);
					break;
		case 6 :	addClass(6, 3, 3, className);
					break;
		case 7 :	addClass(9, 0, 5, className);
					break;
		case 8 :	addClass(8, 0, 5, className);
					break;
		case 9 :	addClass(5, 7, 4, className);
					break;
		case 10 :	addClass(10, 0, 3, className);
					break;
		case 11 :	addClass(8, 5, 6, className);
					break;
		case 12 :	addClass(9, 5, 6, className);
					break;
	}
	if(z>0 && y!=0) {
		y = 6-y;
	}
	switch(y) {
		case 1 	:	addClass(3, 6, 4, className);
					break;
		case 2	:	addClass(4, 5, 3, className);
					break;
		case 3	:	addClass(2, 2, 7, className);
					break;
		case 4	:	addClass(3, 0, 6, className);
					break;
		case 5	:	addClass(3, 0, 10, className);
					break;
	}
	//	Need to add if {} else {} to handle Half Past CASE(for y==0).
	if (y>0) {
	switch(z) {
		case 1	:	addClass(4, 9, 2, className);
					break;
		case 0	:	addClass(5, 0, 4, className);
					break;
	}} else if (y == 0 && z== 1) {	
					addClass(4, 0, 4, className);
					addClass(5, 0, 4, className);
	}
	c = hourString + ' : ' + minString + ' : ' + secsString;
	document.getElementById('txt').value=c;
	t = setTimeout("timedCount()",700);
}

function addClass(row, col, len, className) {
	for (var i=col; i<(col+len); i++)
	{
		id = String(row)+String(i);
		document.getElementById(id).setAttribute('class', className);
	}
}