var app = angular.module("clockApp", []);

app.controller('clockClass', function ($scope, $timeout) {
	$scope.demo_text = 'This is a demo text';

	$scope.clockElement = {
		value: null,
		lighted: false
	};
	$scope.list_row = [
		['I','T','L','I','S','A','S','T','I','M','E'],
		['A','C','Q','U','A','R','T','E','R','D','C'],
		['T','W','E','N','T','Y','F','I','V','E','X'],
		['H','A','L','F','B','T','E','N','F','T','O'],
		['P','A','S','T','E','R','U','N','I','N','E'],
		['O','N','E','S','I','X','T','H','R','E','E'],
		['F','O','U','R','F','I','V','E','T','W','O'],
		['E','I','G','H','T','E','L','E','V','E','N'],
		['S','E','V','E','N','T','W','E','L','V','E'],
		['T','E','N','S','E',"O'",'C','L','O','C','K']
	];

	$scope.hour_map = {
		1: 	[5, 0, 3],
		2: 	[6, 8, 3],
		3: 	[5, 6, 5],
		4: 	[6, 0, 4],
		5:	[6, 4, 4],
		6:	[5, 3, 3],
		7:	[8, 0, 5],
		8:	[7, 0, 5],
		9:	[4, 7, 4],
		10:	[9, 0, 3],
		11:	[7, 5, 6],
		12:	[8, 5, 6]
	};

	$scope.minute_map = {
		1: [2, 6, 4],
		2: [3, 5, 3],
		3: [1, 2, 7],
		4: [2, 0, 6],
		5: [2, 0, 10]
	};

	$scope.reset_matrix = function () {
		$scope.matrix = [];
		$scope.row = [];
		angular.forEach($scope.list_row, function(row) {
			angular.forEach(row, function(elem) {
				clElem = angular.copy($scope.clockElement);
				clElem.value = elem;
				clElem.lighted = false;
				$scope.row.push(
					clElem
				);
			});
	 		$scope.matrix.push($scope.row);
	 		$scope.row = [];
		});
	}


	$scope.lightElement = function(row_count, col_count, len) {
		var x = col_count;
		while (x < col_count + len) {
			$scope.matrix[row_count][x].lighted = true;
			x += 1;
		}
	}

	$scope.timedCount = function() {
		$scope.reset_matrix();
		$scope.lightElement(0, 0, 2);
		$scope.lightElement(0, 3, 2);
		$scope.lightElement(9, 5, 6);
		var
			dateTime = new Date(),
			hour = dateTime.getHours()%12,
			mins = dateTime.getMinutes(),
			secs = dateTime.getSeconds(),
			x = hour,
			y = parseInt((mins%30)/5),
			z = parseInt(mins/30);
			// y = parseInt((secs%30)/5),
			// z = parseInt(secs/30);

		if (z>0 && y!=0)
		{
			x +=1;
		}
		x = x == 0 ? 12 : x;

		var hour_elements = $scope.hour_map[x];

		$scope.lightElement(hour_elements[0], hour_elements[1], hour_elements[2]);

		if(z>0 && y!=0) {
			y = 6-y;
		}

		if (y !=0 ) {
			var minute_elements = $scope.minute_map[y];
			$scope.lightElement(minute_elements[0], minute_elements[1], minute_elements[2]);
		}

		if (y>0) {
			switch(z) {
				case 1	:	$scope.lightElement(3, 9, 2);
							break;
				case 0	:	$scope.lightElement(4, 0, 4);
							break;
			}
		} else if (y == 0 && z== 1) {	
			$scope.lightElement(3, 0, 4);
			$scope.lightElement(4, 0, 4);
		}
		$timeout(function () { $scope.timedCount(); }, 990);
	}
	$timeout(function () { $scope.timedCount(); }, 990);
});